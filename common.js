
// Set the default timeout to 1 minute, since the backend experience can be
// quite slow. If you change this value in a test suite, please set it back.
casper.options.waitTimeout = 60000;

/**
 * Listen to the open.location event in order to prepend the hostname.
 *
 * This filter will automatically prepend the full hostname that you are running
 * tests against to a given path. For example, if you run
 * casper.thenOpen('some-page'), it will convert it to
 * http://example.com/some-page
 */
casper.setFilter('open.location', function(location) {
  if (typeof location === 'undefined') {
    location = "";
  }

  // If the URL is already absolute, just return it unmodified.
  if (location.search(/^http/) != -1) {
    return location;
  }

  var cleanPath = location.replace(/^\//, '');
  return casper.cli.get('url') + '/' + cleanPath;
});

/**
 * Save page markup to a file. Respect an existing savePageContent function, if
 * casper.js core introduces one.
 *
 * @param String targetFile
 *   A target filename.
 * @return Casper
 */
casper.savePageContent = casper.savePageContent || function(targetFile) {
  var fs = require('fs');

  // Get the absolute path.
  targetFile = fs.absolute(targetFile);
  // Let other code modify the path.
  targetFile = this.filter('page.target_filename', targetFile) || targetFile;
  this.log(f("Saving page html to %s", targetFile), "debug");
  // Try saving the file.
  try {
    fs.write(targetFile, this.getPageContent(), 'w');
  } catch(err) {
    this.log(f("Failed to save page html to %s; please check permissions", targetFile), "error");
    this.log(err, "debug");
    return this;
  }

  this.log(f("Page html saved to %s", targetFile), "info");
  // Trigger the page.saved event.
  this.emit('page.saved', targetFile);

  return this;
};

/**
 * Generates a random string of ASCII characters of codes 32 to 126.
 *
 * The generated string includes alpha-numeric characters and common
 * miscellaneous characters. Use this method when testing general input where
 * the content is not restricted.
 *
 * Do not use this method when special characters are not possible (e.g., in
 * machine or file names that have already been validated); instead, use
 * casper.randomName().
 *
 * @param Number length
 *   Length of random string to generate.
 *
 * @return String
 *   Randomly generated string.
 */
casper.randomString = casper.randomString || function(length) {
  length = length || 8;
  var str = '';
  for (i = 0; i < length; i++) {
    str += String.fromCharCode(32 + Math.floor((Math.random() * 95)));
  }
  return str;
};

/**
 * Generates a random string containing letters and numbers.
 *
 * The string will always start with a letter. The letters may be upper or
 * lower case. This method is better for restricted inputs that do not accept
 * certain characters. For example, when testing input fields that require
 * machine readable values (i.e. without spaces and non-standard characters)
 * this method is best.
 *
 * Do not use this method when testing unvalidated user input. Instead,
 * use casper.randomString().
 *
 * @param Number length
 *   Length of random string to generate.
 *
 * @return String
 *   Randomly generated string.
 */
casper.randomName = casper.randomName || function(length) {
  length = length || 8;
  var str = String.fromCharCode(97 + Math.floor((Math.random() * 26)));
  var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  for (i = 1; i < length; i++) {
    str += chars.charAt(Math.floor(Math.random() * chars.length));
  }
  return str;
};
