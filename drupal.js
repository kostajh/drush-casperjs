/**
 * CasperJS Drupal helpers.
 */

var utils = require('utils');
var cookieName;

/**
 * An object of user session cookies, keyed by unique name, ie, editor, writer.
 */
casper.drupalSessions = {};

/**
 * Sign in a user using a set of credentials.
 *
 * @param string name
 *   Name of the user to sign in.
 * @param string password
 *   Password to use to authenticate the user.
 *
 * @ingroup drupal_sessions
 */
casper.drupalSignIn = function(name, password) {
  // End any existing session in the browser, but it keep it stored.
  casper.drupalEndSession();

  // If we already have a session going for this user we can just switch to it.
  if (utils.isObject(casper.drupalSessions[name])) {
    casper.drupalBeginSession(name);
  }
  // Otherwise we need to log the user in and store the session info.
  else {
    casper.thenOpen('user', function () {
      this.fill('form#user-login', {
        "name": name,
        "pass": password
      }, true);
    });

    casper.waitForSelector('body.logged-in', function() {
      this.log('Logged in as ' + name, 'info');
    }, function timeout() {
      this.test.fail('Unable to log in as ' + name);
    });

    // Store the session cookie.
    casper.drupalSaveSession(name);
    casper.drupalBeginSession(name);
  }
};

/**
 * Log a user out.
 *
 * Simple wrapper for casper.drupalEndSession().
 */
casper.drupalLogout = function() {
  casper.drupalEndSession();
}

/**
 * Creates a session for a user.
 *
 * @param string name
 *   Name of the user to sign in.
 * @param string password
 *   Password to use to authenticate the user.
 *
 * @ingroup drupal_sessions
 */
casper.drupalSaveSession = function(name) {
  casper.then(function () {
    casper.each(casper.page.cookies, function (self, cookie) {
      if (cookie.name.match(/^SESS/)) {
        // Store the cookie.
        casper.drupalSessions[name] = cookie;
      }
    });
  });

  casper.thenOpen('user', function () {
    var success = this.exists('body.logged-in') && utils.isObject(casper.drupalSessions[name]);
    this.test.assert(success, name + ' session cookie has been stored.');
  });
};

/**
 * Begin a session as a user specified by key.
 *
 * @param string name
 *   The name of the user.
 *
 * @ingroup drupal_sessions
 */
casper.drupalBeginSession = function(name) {
  casper.then(function() {
    if (utils.isUndefined(casper.drupalSessions[name])) {
      this.test.fail("Unable to find session for user " + name);
    }

    // Swap the cookie for our current session cookie.
    casper.page.addCookie(casper.drupalSessions[name]);
    cookieName = casper.drupalSessions[name].name;

    this.log('Began session as ' + name, 'info');
  });
};

/**
 * End a session, switching back to an anonymous user.
 *
 * @ingroup drupal_sessions
 */
casper.drupalEndSession = function() {
  casper.then(function() {
    this.log('COOKIE: ' + cookieName);
    casper.page.deleteCookie(cookieName);
    this.log('Ended authenticated session.', 'info');
  });
};

/**
 * Fill in a Drupal autocomplete field.
 */
casper.drupalFillAutocomplete = function(field, value) {
  this.thenEvaluate(function(js_field, js_value) {
    // First set some value in the field.
    jQuery(js_field).val(js_value);
    // Then type something to trigger the autocomplete handler.
    jQuery(js_field).trigger('keyup');
  }, {js_field: field, js_value: value});

  // Wait for the autocomplete suggestions to load and choose the first one.
  // This isn't perfect, but it should suffice as long as values are unique
  // enough.
  this.waitForSelector('#autocomplete li');

  return this.thenEvaluate(function(js_field) {
      jQuery('#autocomplete li:first').trigger('mouseover');
      jQuery('#autocomplete li:first').trigger('mousedown');
      jQuery('#autocomplete').hide();;
      return true;
    }, {js_field: field})
    .then(function() {
      return true;
    });
};
